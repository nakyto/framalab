/* eslint-disable import/no-commonjs */
/* eslint-disable import/no-extraneous-dependencies */

const Feed = require('feed').Feed; // eslint-disable-line
const fs = require('fs');
const marked = require('marked');

function buildFeeds(env, wiki) {
  env.translations.available.forEach((lg) => {
    if (wiki[lg] !== undefined) {
      if (Object.keys(wiki[lg]).includes('news')) {
        const type = 'news';

        const feed = new Feed({
          author: env.meta.author,
          copyright: env.meta.copyright || '',
          description: env.meta.description || '',
          favicon: `${env.url}img/icons/favicon.png`,
          generator: 'Vue-Fs',
          id: env.url,
          image: `${env.url}img/opengraph/${type}.jpg`,
          lg,
          link: env.url,
          title: env.meta.title || '',
          updated: new Date(),
        });

        Object.keys(wiki[lg][type].posts)
          .slice() //      shallow copy of the array
          .sort() //       posts sorted
          .reverse() //    with latest in first
          .slice(0, 5) //  and only 5 posts kept in the feed
          .forEach((date) => {
            feed.addItem({
              content: marked(wiki[lg][type].posts[date].text),
              date: new Date(`20${date}`),
              id: wiki[lg][type].posts[date].url,
              link: wiki[lg][type].posts[date].url,
              title: wiki[lg][type].posts[date].title,
            });
          });

        fs.mkdirSync(`./dist${env.basepath}${lg}/${type}/`, { recursive: true });
        fs.writeFileSync(
          `./dist${env.basepath}${lg}/${type}/feed.xml`,
          feed.rss2(),
        );
      }
    }
  });
}

module.exports = buildFeeds;
